import 'package:flutter/material.dart';



class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
                colors: [
                  Colors.blue,
                  Colors.lightBlueAccent.shade100,
                ],
                //stops: [0.0, 1.0],
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                tileMode: TileMode.repeated
            )
        ),
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
