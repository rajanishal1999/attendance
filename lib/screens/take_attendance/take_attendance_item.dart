import 'package:attendance/provider/validate_attendance.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class TakeAttendanceItem extends StatefulWidget {
  final String rollNo;
  final String name;
  TakeAttendanceItem({this.rollNo, this.name});
  @override
  _TakeAttendanceItemState createState() => _TakeAttendanceItemState();
}

class _TakeAttendanceItemState extends State<TakeAttendanceItem> {
  String _selectedValue = '';
  var _isItemSelected = false;



  _addAttendance(String value) {
    _isItemSelected = true;
    Provider.of<ValidateAttendance>(context, listen: false).addAttendance(value, widget.name, widget.rollNo);

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: !_isItemSelected ? Theme.of(context).cardColor : Colors.white.withAlpha(0),
      margin: EdgeInsets.only(bottom: 12),
      padding: EdgeInsets.all(8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(widget.rollNo, style: TextStyle(fontSize: 16),),
                Text(widget.name, style: TextStyle(color: Colors.grey, fontSize: 14),),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: InkWell(
                  onTap: () {
                    _addAttendance('P');
                    setState(() {
                      _selectedValue = 'P';
                    });
                  },
                  child: CircleAvatar(
                    backgroundColor: _selectedValue == 'P' ? Theme.of(context).accentColor :Colors.grey,
                    child: Text('P', style: TextStyle(color: Theme.of(context).cardColor),),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: InkWell(
                  onTap: () {
                    _addAttendance('A');
                    setState(() {
                      _selectedValue = 'A';
                    });
                  },
                  child: CircleAvatar(
                    backgroundColor: _selectedValue == 'A' ? Theme.of(context).accentColor :Colors.grey,
                    child: Text('A', style: TextStyle(color: Theme.of(context).cardColor),),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: InkWell(
                  onTap: () {
                    _addAttendance('N');
                    setState(() {
                      _selectedValue = 'N';
                    });
                  },
                  child: CircleAvatar(
                    backgroundColor: _selectedValue == 'N' ? Theme.of(context).accentColor :Colors.grey,
                    child: Text('N', style: TextStyle(color: Theme.of(context).cardColor),),
                  ),
                ),
              ),
            ],
          ),

        ],
      ),
    );
  }
}
