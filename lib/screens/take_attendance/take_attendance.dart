import 'package:attendance/provider/validate_attendance.dart';
import 'package:attendance/screens/take_attendance/take_attendance_item.dart';
import 'package:attendance/widgets/take_attendance/attendance_report.dart';
import 'package:attendance/widgets/take_attendance/attendance_time.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';



class TakeAttendance extends StatelessWidget {
  final String name;
  final String description;
  final String createdOn;
  final String docId;
  final String currentUserId;
  final int colorValue;
  TakeAttendance({this.currentUserId, this.docId, this.name, this.createdOn, this.description, this.colorValue});



  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => ValidateAttendance(),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Take Attendance'),
        ),
        body: StreamBuilder(
          stream: Firestore.instance.collection('students').document(currentUserId).collection('students_list').where('classroomDocId', isEqualTo: docId).orderBy('student_roll_number', descending: false).snapshots(),
          builder: (BuildContext ctx,AsyncSnapshot<QuerySnapshot> snapshot) {
            if(snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator(),);
            }
            if(snapshot.data.documents.length == 0) {
              return Center(child: Text('No Student Record Available'),);
            }
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  AtttendanceTime(),
                  Container(
                    margin: EdgeInsets.only(top: 12),
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (ctx, index) {
                        final studentname = snapshot.data.documents[index]['student_name'];
                        final rollNo = snapshot.data.documents[index]['student_roll_number'].toString();
                        return TakeAttendanceItem(
                          name: studentname,
                          rollNo: rollNo,
                        );
                      },
                    ),
                  ),
                  AttendanceValidateReport(
                    totalNoStudent: snapshot.data.documents.length,
                    currentUserId: currentUserId,
                    classroomId: docId,
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

}
