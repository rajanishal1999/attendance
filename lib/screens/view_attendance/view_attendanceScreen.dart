import 'package:attendance/screens/view_attendance/view_attendance_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class ViewAttendance extends StatefulWidget {
  final String currentUid;
  final String classroomId;
  ViewAttendance({this.classroomId, this.currentUid});

  @override
  _ViewAttendanceState createState() => _ViewAttendanceState();
}

class _ViewAttendanceState extends State<ViewAttendance> {
  String _searchValue = 'all';


  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('View Attendance'),
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Container(
              height: 50,
              margin: EdgeInsets.only(top: 8),
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 4, left: 4),
                    child: GestureDetector(
                      onTap: () {
                        if(_searchValue != 'all') {
                          setState(() {
                            _searchValue = 'all';
                          });
                        }
                      },
                      child: Chip(
                        avatar: _searchValue == 'all'  ? Icon(Icons.done) : null,
                        backgroundColor: _searchValue == 'all'  ?Colors.white : null,
                        shape: _searchValue == 'all'  ? StadiumBorder(side: BorderSide()) : null,
                        label: Text('All'),

                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 4, left: 4),
                    child: GestureDetector(
                      onTap: () {
                        if(_searchValue != 'last1hour') {
                          setState(() {
                            _searchValue = 'last1hour';
                          });
                        }
                      },
                      child: Chip(
                        avatar: _searchValue == 'last1hour'  ? Icon(Icons.done) : null,
                        backgroundColor: _searchValue == 'last1hour'  ?Colors.white : null,
                        shape: _searchValue == 'last1hour'  ? StadiumBorder(side: BorderSide()) : null,
                        label: Text('This Hour'),

                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 4, left: 4),
                    child: GestureDetector(
                      onTap: () {
                        if(_searchValue != 'today') {
                          setState(() {
                            _searchValue = 'today';
                          });
                        }
                      },
                      child: Chip(
                        avatar: _searchValue == 'today'  ? Icon(Icons.done) : null,
                        backgroundColor: _searchValue == 'today'  ?Colors.white : null,
                        shape: _searchValue == 'today'  ? StadiumBorder(side: BorderSide()) : null,
                        label: Text('Today'),

                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 4, left: 4),
                    child: GestureDetector(
                      onTap: () {
                        if(_searchValue != 'yesterday') {
                          setState(() {
                            _searchValue = 'yesterday';
                          });
                        }
                      },
                      child: Chip(
                        avatar: _searchValue == 'yesterday'  ? Icon(Icons.done) : null,
                        backgroundColor: _searchValue == 'yesterday'  ?Colors.white : null,
                        shape: _searchValue == 'yesterday'  ? StadiumBorder(side: BorderSide()) : null,
                        label: Text('Yesterday'),

                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 4, left: 4),
                    child: GestureDetector(
                      onTap: () {
                        if(_searchValue != 'before7days') {
                          setState(() {
                            _searchValue = 'before7days';
                          });
                        }
                      },
                      child: Chip(
                        avatar: _searchValue == 'before7days'  ? Icon(Icons.done) : null,
                        backgroundColor: _searchValue == 'before7days'  ?Colors.white : null,
                        shape: _searchValue == 'before7days'  ? StadiumBorder(side: BorderSide()) : null,
                        label: Text('This Week'),

                      ),
                    ),
                  ),

                ],
              ),
            ),
            StreamBuilder(
              stream: Firestore.instance.collection('attendance').document(widget.currentUid).collection('attendance_list').where('classroomId', isEqualTo: widget.classroomId).orderBy('cretedAt', descending: true).snapshots(),
              builder: (BuildContext ctx,AsyncSnapshot<QuerySnapshot> snapshot) {
                if(snapshot.connectionState == ConnectionState.waiting) {
                  //return Center(child: CircularProgressIndicator(),);
                  return Column(
                    children: [
                      SizedBox(
                        height: screenSize.height / 2.5 ,
                      ),
                      Center(
                        child: CircularProgressIndicator(),
                      ),
                    ],
                  );
                }
                if(snapshot.data.documents.length == 0) {
                  return Center(child: Text('No Attendance Record Available'),);
                }
                return Container(
                  margin: EdgeInsets.only(top: 4),
                  child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (ctx, index) {
                      final dateTimeObj = DateTime.parse(snapshot.data.documents[index]['attendanceTime']);
                      final ctime = DateTime.now();
                      final attendanceTaken = DateFormat('hh:mm a - dd/MM/yyyy').format(DateTime.parse(snapshot.data.documents[index]['attendanceTime']));
                      final attendanceData = snapshot.data.documents[index]['data'];
                      final List<int> keyLst = [];
                      attendanceData.forEach((key, value) {
                        keyLst.add(int.parse(key));
                      });
                      keyLst.sort();
                      if(_searchValue == 'all') {
                        return AttendanceViewItem(
                          colorValue: snapshot.data.documents[index]['colorValue'],
                          dateTimeValue: attendanceTaken,
                          totalNoOfAbsent: snapshot.data.documents[index]['noOfAbsent'],
                          totalNoOfNotcame: snapshot.data.documents[index]['noOfNotCame'],
                          totalNoOfStudent: snapshot.data.documents[index]['totalNoOfStudents'],
                          totalNoOfPresent: snapshot.data.documents[index]['noOfPresent'],
                          dataKey: keyLst,
                          rawDataMap: attendanceData,
                          docId: snapshot.data.documents[index].documentID.toString(),
                        );
                      }
                      if(_searchValue == 'last1hour') {
                        if(ctime.year == dateTimeObj.year &&ctime.month == dateTimeObj.month && ctime.day == dateTimeObj.day && ctime.hour == dateTimeObj.hour) {
                          ///reture one houe attendance
                          return AttendanceViewItem(
                            colorValue: snapshot.data.documents[index]['colorValue'],
                            dateTimeValue: attendanceTaken,
                            totalNoOfAbsent: snapshot.data.documents[index]['noOfAbsent'],
                            totalNoOfNotcame: snapshot.data.documents[index]['noOfNotCame'],
                            totalNoOfStudent: snapshot.data.documents[index]['totalNoOfStudents'],
                            totalNoOfPresent: snapshot.data.documents[index]['noOfPresent'],
                            dataKey: keyLst,
                            rawDataMap: attendanceData,
                            docId: snapshot.data.documents[index].documentID.toString(),
                          );
                        }
                        else {
                          return SizedBox(height: 0.0,);
                        }
                      }
                      if(_searchValue == 'today') {
                        if(ctime.year == dateTimeObj.year &&ctime.month == dateTimeObj.month && ctime.day == dateTimeObj.day) {
                          ///reture today attendance
                          return AttendanceViewItem(
                            colorValue: snapshot.data.documents[index]['colorValue'],
                            dateTimeValue: attendanceTaken,
                            totalNoOfAbsent: snapshot.data.documents[index]['noOfAbsent'],
                            totalNoOfNotcame: snapshot.data.documents[index]['noOfNotCame'],
                            totalNoOfStudent: snapshot.data.documents[index]['totalNoOfStudents'],
                            totalNoOfPresent: snapshot.data.documents[index]['noOfPresent'],
                            dataKey: keyLst,
                            rawDataMap: attendanceData,
                            docId: snapshot.data.documents[index].documentID.toString(),
                          );
                        }
                        else {
                          return SizedBox(height: 0.1,);
                        }
                      }
                      if(_searchValue == 'yesterday') {
                        if(ctime.year == dateTimeObj.year &&ctime.month == dateTimeObj.month && ctime.day -1 == dateTimeObj.day) {
                          ///reture yeaterday attendance
                          return AttendanceViewItem(
                            colorValue: snapshot.data.documents[index]['colorValue'],
                            dateTimeValue: attendanceTaken,
                            totalNoOfAbsent: snapshot.data.documents[index]['noOfAbsent'],
                            totalNoOfNotcame: snapshot.data.documents[index]['noOfNotCame'],
                            totalNoOfStudent: snapshot.data.documents[index]['totalNoOfStudents'],
                            totalNoOfPresent: snapshot.data.documents[index]['noOfPresent'],
                            dataKey: keyLst,
                            rawDataMap: attendanceData,
                            docId: snapshot.data.documents[index].documentID.toString(),
                          );
                        }
                        else {
                          return SizedBox(height: 0.1,);
                        }
                      }
                      if(_searchValue == 'before7days') {
                        if(ctime.year == dateTimeObj.year &&ctime.month == dateTimeObj.month && ctime.day - 7 == dateTimeObj.day) {
                          ///reture thisweek attendance
                          return AttendanceViewItem(
                            colorValue: snapshot.data.documents[index]['colorValue'],
                            dateTimeValue: attendanceTaken,
                            totalNoOfAbsent: snapshot.data.documents[index]['noOfAbsent'],
                            totalNoOfNotcame: snapshot.data.documents[index]['noOfNotCame'],
                            totalNoOfStudent: snapshot.data.documents[index]['totalNoOfStudents'],
                            totalNoOfPresent: snapshot.data.documents[index]['noOfPresent'],
                            dataKey: keyLst,
                            rawDataMap: attendanceData,
                            docId: snapshot.data.documents[index].documentID.toString(),
                          );
                        }
                        else {
                          return SizedBox(height: 0.1,);
                        }
                      }
                      return SizedBox(height: 0.1,);
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
