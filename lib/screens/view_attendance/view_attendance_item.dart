import 'package:attendance/widgets/global_widgets.dart';
import 'package:flutter/material.dart';




class AttendanceViewItem extends StatelessWidget {
  final String dateTimeValue;
  final int colorValue;
  final int totalNoOfStudent;
  final int totalNoOfPresent;
  final int totalNoOfAbsent;
  final int totalNoOfNotcame;
  final List<int> dataKey;
  final dynamic rawDataMap;
  final String docId;
  AttendanceViewItem({this.colorValue, this.totalNoOfStudent, this.dateTimeValue, this.totalNoOfAbsent, this.totalNoOfNotcame, this.totalNoOfPresent, this.dataKey, this.rawDataMap, this.docId});


  showDetailView(BuildContext context) {
    Navigator.push(context, new MaterialPageRoute(
        fullscreenDialog: true,
        builder: (BuildContext context) {
          return  Hero(
            tag: docId,
            child: Scaffold(
              appBar:  AppBar(
                title:  Text('Attendance Details'),
                //centerTitle: true,
              ),
              body:  SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 25),
                    Wrap(
                      spacing: 8.0,
                      runSpacing: 4.0,
                      children: <Widget>[
                        Chip(
                          avatar: Icon(Icons.group),
                          label: Text('Total Students $totalNoOfStudent'),
                        ),
                        Chip(
                          avatar: Icon(Icons.done),
                          label: Text('Present $totalNoOfPresent'),
                        ),
                        Chip(
                          avatar: Icon(Icons.close),
                          label: Text('Absent $totalNoOfAbsent'),
                        ),
                        Chip(
                          avatar: Icon(Icons.compare_arrows_sharp),
                          label: Text('Not came $totalNoOfNotcame'),
                        ),
                      ],
                    ),
                    ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: dataKey.length,
                      itemBuilder: (ctx, index) => ListTile(
                        title: Text(rawDataMap[dataKey[index].toString()]['studentName']),
                        subtitle: Text(rawDataMap[dataKey[index].toString()]['rollNo']),
                        leading: CircleAvatar(
                          backgroundColor: randomColorsToBeChoose[colorValue],
                          child: Text(rawDataMap[dataKey[index].toString()]['studentValue']),
                        ),
                      ),
                    ),
                    SizedBox(height: 18),
                  ],
                ),
              ),
            ),
          );

        }
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      color: Theme.of(context).cardColor,
      child: ListTile(
        onTap: () {
          showDetailView(context);
        },
        leading: Hero(
          tag: docId,
          child: CircleAvatar(
            child: Padding(
              padding: const EdgeInsets.all(4),
              child: Icon(totalNoOfStudent == totalNoOfPresent ? Icons.done : Icons.close),
            ),
            backgroundColor: randomColorsToBeChoose[colorValue],
          ),
        ),
        title: Text(dateTimeValue),
        subtitle: Text('Present: $totalNoOfPresent  Absent: $totalNoOfAbsent  NotCame: $totalNoOfNotcame'),
      ),
    );
  }


}
