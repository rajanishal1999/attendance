import 'package:attendance/items/classroom_Item.dart';
import 'package:attendance/provider/long_press_provider.dart';
import 'package:attendance/screens/splah_screen.dart';
import 'package:attendance/widgets/add_classroom.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';



class HomePage extends StatelessWidget {
  final String currentUid;
  HomePage({this.currentUid});


  void editClassrommAction(BuildContext context, String name, String description, String docId, int itemColor) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (bctx) {
        return GestureDetector(
          onTap: () {},
          behavior: HitTestBehavior.opaque,
          child: AddClassroom(isEdit: true, receivedName: name, receivedDes: description, docId: docId, tileColor: itemColor,),
        );
      },
    );
  }

  void addClassrommAction(BuildContext ctx,  var isEdit, String name, String des, String docId) {
   showModalBottomSheet(
     isScrollControlled: true,
     context: ctx,
     builder: (bctx) {
       return GestureDetector(
         onTap: () {},
         behavior: HitTestBehavior.opaque,
         child: AddClassroom(isEdit: isEdit, receivedName: name, receivedDes: des, docId: docId,),
       );
     },
   );
 }

  void deleteClassroom(BuildContext context, List<String> docLst) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text('Delete', style: TextStyle( color: Theme.of(context).errorColor),),
        content: Text('Deleting this classroom will delete all students record in the classroom'),
        actions: <Widget>[
          TextButton(
            child: Text('No'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          ),
          TextButton(
            child: Text('Yes'),
            onPressed: () {
              Navigator.of(ctx).pop();
              for(String docItem in docLst) {
                Firestore.instance.collection('classrooms').document(currentUid).collection('classroom_list').document(docItem).delete();
              }
            },
          ),
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => LongPressProvider(),
      child: StreamBuilder(
        stream: Firestore.instance.collection('users').document(currentUid).snapshots(),
        builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> mainSnapshot) {
          if(mainSnapshot.connectionState == ConnectionState.waiting) {
            return SplashScreen();
          }
          return Scaffold(
            endDrawerEnableOpenDragGesture: false,
            appBar: PreferredSize(
              preferredSize: Size(50, 50),
              child: Consumer<LongPressProvider>(
                builder: (ctx, longP, _) {
                  return AppBar(
                    backwardsCompatibility: false,
                    systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.blue),
                    backgroundColor: Theme.of(context).primaryColor,
                    title: Text(longP.getLongPress ? '${longP.getdeleteBtnLst().length} Selected' :'Your Classrooms'),
                    leading: IconButton(
                      icon: Icon(longP.getLongPress ? Icons.close : Icons.menu),
                      onPressed: () {
                        if(longP.getLongPress) {
                          longP.stopLongPress();
                        }else {
                          Scaffold.of(ctx).openDrawer();
                        }

                      },
                    ),
                    actions: <Widget>[
                      if(longP.isEditButton())
                        IconButton(
                          icon: Icon(Icons.edit),
                          onPressed: () {
                            Map<String, String> dataofEdit = longP.getEditBtnData;
                            editClassrommAction(ctx, dataofEdit['name'], dataofEdit['des'], dataofEdit['docId'], int.parse(dataofEdit['tileColor']));
                            longP.stopLongPress();
                          },
                        ),
                      if(longP.getLongPress)
                        IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () {
                            deleteClassroom(context, longP.getdeleteBtnLst());
                            longP.stopLongPress();
                          },
                        ),
                    ],
                  );
                },

              ),
            ),
            drawer: DrawerMenu(currentUid: currentUid, mainSnapshot: mainSnapshot,),
            floatingActionButton: FloatingActionButton(
              tooltip: 'Create new Classroom',
              child: Icon(Icons.add_to_photos_sharp),
              onPressed: () {
                addClassrommAction(context, false, null, null, null);
              },

            ),
            body: StreamBuilder(
              stream: Firestore.instance.collection('classrooms').document(currentUid).collection('classroom_list').orderBy('CreatedAt', descending: true).snapshots(),
              builder: (BuildContext ctx,AsyncSnapshot<QuerySnapshot> snapshots) {
                if(snapshots.connectionState == ConnectionState.done && snapshots.data.documents.length == 0) {
                  return Center(
                    child: Text('No Classroom available Create One'),
                  );
                }
                if(snapshots.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }

                return SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          itemCount: snapshots.data.documents.length,
                          itemBuilder: (ctx, index) {
                            final tileColor = snapshots.data.documents[index]['colorValue'];
                            final itemName = snapshots.data.documents[index]['classroom_name'];
                            final itemDescription = snapshots.data.documents[index]['classroom_description'];
                            final timeStamp = DateFormat('dd/MM/yy-hh:mm a').format(DateTime.parse(snapshots.data.documents[index]['iso8601timestamp']));
                            final itemDocId = snapshots.data.documents[index].documentID;
                            return ClassroomItem(
                              docId: itemDocId,
                              itemName: itemName,
                              itemColor: tileColor,
                              cretedTime: timeStamp,
                              itemDescription: itemDescription,
                              currentUserId: currentUid,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }



}

class DrawerMenu extends StatelessWidget {
  // const DrawerMenu({
  //   Key key,
  //   @required this.currentUid,
  // }) : super(key: key);

  final String currentUid;
  final dynamic mainSnapshot;

  DrawerMenu({this.currentUid, this.mainSnapshot});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Row(
              children: <Widget>[
                Container(
                  width: 100,
                  height: 100,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(70),
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/images/noimg.png',
                      image: mainSnapshot.data['image_url'],
                      fit: BoxFit.cover,
                    ),
                  ),
                ),

                SizedBox(width: 20),
                Text(mainSnapshot.data['username'], style: TextStyle(
                  color: Theme.of(context).cardColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                ), overflow: TextOverflow.ellipsis,)
              ],
            ),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
          ),
          ListTile(
            leading: Icon(Icons.panorama_horizontal_select, color: Theme.of(context).primaryColor,),
            title: Text('Your Classrooms'),
            onTap: () {
              if(!ModalRoute.of(context).isActive) {
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) =>  HomePage(
                          currentUid: currentUid,
                        )));
              }
              Navigator.of(context).pop();

            },
          ),
          Divider(
            height: 1,
          ),
          ListTile(
            leading: Icon(Icons.person, color: Theme.of(context).primaryColor,),
            title: Text('Profile'),
            onTap: () {
              showModalBottomSheet(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(25.0)),
                ),
                context: context,
                builder: (bctx) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(mainSnapshot.data['username'], style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                        ),),
                        subtitle: Text(mainSnapshot.data['email']),
                      ),
                      Container(
                        child: Image.network(mainSnapshot.data['image_url']),
                        margin: EdgeInsets.symmetric(horizontal: 10),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          Divider(
            height: 1,
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app, color: Theme.of(context).errorColor,),
            title: Text('Logout'),
            onTap: () {
              showDialog(context: context, builder: (ctx) => AlertDialog(
                title: Text('Logout'),
                content: Text('Are you sure you want to logout'),
                actions: <Widget>[
                  TextButton(onPressed: () {
                    Navigator.of(context).pop();
                  }, child: Text('cancel')),
                  TextButton(onPressed: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.of(context).pop();
                  }, child: Text('Ok')),
                ],
              ));
            },
          ),
          Divider(
            height: 1,
          ),
          SizedBox(height: 18,),
          ListTile(
            subtitle: Text('Version V1.01'),
          ),
        ],
      ),
    );
  }
}
