import 'package:attendance/provider/long_press_provider.dart';
import 'package:attendance/widgets/global_widgets.dart';
import 'package:attendance/widgets/student/add_student.dart';
import 'package:attendance/widgets/student/student_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';


class StudentScreen extends StatelessWidget {
  final String name;
  final String decription;
  final String createdon;
  final int tileColor;
  final String classroomDocId;
  final String currentUserId;
  final int appBarColor;
  StudentScreen({this.name, this.createdon, this.decription, this.tileColor, this.classroomDocId, this.currentUserId, this.appBarColor});




  void addStudentAction(BuildContext ctx,  var isEdit) {
    showModalBottomSheet(
      context: ctx,
      isScrollControlled: true,
      builder: (bctx) {
        return GestureDetector(
          onTap: () {},
          behavior: HitTestBehavior.opaque,
          child: AddStudent(isEdit: isEdit, tileColor: null, docId: classroomDocId, receivedName: null, receivedRoll: null,),
        );
      },
    );
  }


  void _editStudentAction(BuildContext context, String name, String rollNo, int tileColor, String docId) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (bctx) {
        return GestureDetector(
          onTap: () {},
          behavior: HitTestBehavior.opaque,
          child: AddStudent(isEdit: true, tileColor: tileColor, docId: docId, receivedName: name, receivedRoll: rollNo,),
        );
      },
    );
  }


  void _deleteStudent(BuildContext context, String currentUserId, List<String> docIdLst) {
    showDialog(context: context, builder: (ctx) => AlertDialog(
      title: Text('Delete', style: TextStyle(color: Theme.of(context).errorColor),),
      content: Text('This will Delete the Student in the classroom'),
      actions: <Widget>[
        TextButton(onPressed: () {
          Navigator.of(ctx).pop();
        }, child: Text('Cancel')),
        TextButton(onPressed: () async {
          Navigator.of(ctx).pop();
          for(String docIdItem in docIdLst) {
            await Firestore.instance.collection('students').document(currentUserId).collection('students_list').document(docIdItem).delete();
          }

        }, child: Text('Ok')),
      ],
    ));
  }


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => LongPressProvider(),
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(50, 50),
          child: Consumer<LongPressProvider>(
            builder: (ctx, longP, _) {
              return AppBar(
                title: Text(longP.getLongPress ? 'Select' : 'Student Details'),
                leading: IconButton(
                    icon: Icon(longP.getLongPress ? Icons.close : Icons.arrow_back),
                    onPressed: () {
                      if(longP.getLongPress) {
                        longP.stopLongPress();
                      }
                      else {
                        longP.dispose();
                        //Provider.of<LongPressProvider>(context).dispose();
                        Navigator.of(context).pop();
                      }
                    }
                ),
                actions: <Widget>[
                  if(longP.isEditButton())
                    IconButton(icon: Icon(Icons.edit), onPressed: () {
                      _editStudentAction(context, longP.getEditBtnData['name'], longP.getEditBtnData['rollNo'], int.parse(longP.getEditBtnData['tileColor']), longP.getEditBtnData['DocId']);
                      longP.stopLongPress();
                    }),
                  if(longP.getLongPress)
                    IconButton(icon: Icon(Icons.delete), onPressed: () {
                      _deleteStudent(context, currentUserId, longP.getdeleteBtnLst());
                      longP.stopLongPress();
                    }),

                ],
              );
            },

          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.person_add),
          onPressed: () {
            addStudentAction(context, false);
          },
        ),
        body: StreamBuilder(
          stream: Firestore.instance.collection('students').document(currentUserId).collection('students_list').where('classroomDocId', isEqualTo: classroomDocId).orderBy('student_roll_number', descending: false).snapshots(),
          builder: (BuildContext ctx,AsyncSnapshot<QuerySnapshot> snapshot) {
            if(snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator(),);
            }
            if(snapshot.data.documents.length == 0) {
              return Center(child: Text('No Student Record Available'),);
            }
            return SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  SizedBox(height: 8,),
                  Container(
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (ctx, index) {
                        final colorValue = snapshot.data.documents[index]['colorValue'];
                        final studentname = snapshot.data.documents[index]['student_name'];
                        final rollNo = snapshot.data.documents[index]['student_roll_number'].toString();
                        final docId = snapshot.data.documents[index].documentID;
                        final createdOn = DateFormat('dd/MM/yy-hh:mm a').format(DateTime.parse(snapshot.data.documents[index]['iso8601timestamp']));
                        return StudentItem(
                          tileColor: colorValue,
                          name: studentname,
                          rollNo: rollNo,
                          createdOn: createdOn,
                          currentUserId: currentUserId,
                          docId: docId,
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
