
import 'package:attendance/screens/auth_screen.dart';
import 'package:attendance/screens/home_page.dart';
import 'package:attendance/screens/splah_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyAppMain());
}



class MyAppMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
         //brightness: Brightness.dark,
          primarySwatch: Colors.pink,
          primaryColor: Colors.blue,
          accentColor: Colors.pink,
        errorColor: Colors.red,
        cardColor: Colors.white,
        scaffoldBackgroundColor: Colors.white.withAlpha(500),
      ),
      home: StreamBuilder(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (ctx,AsyncSnapshot<FirebaseUser> userSnapshot) {
          if(userSnapshot.connectionState == ConnectionState.waiting){
            return SplashScreen();
          }
          if (userSnapshot.hasData) {
            return HomePage(
              currentUid: userSnapshot.data.uid,
            );
          }
          return AuthScreen();
        },
      ),
    );
  }
}








