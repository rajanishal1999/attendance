import 'package:flutter/material.dart';



var randomColorsToBeChoose = [
  Colors.green,
  Colors.pink,
  Colors.amber,
  Colors.blue,
  Colors.red,
  Colors.brown,
  Colors.blueGrey,
  Colors.deepOrange,
  Colors.deepPurple,
  Colors.purple,
];



List<String> actionImageList = [
  'assets/images/allstudents.png',
  'assets/images/takeattendance.png',
  'assets/images/attendance.png',
];

List<Widget> actionIconList = [
  Icon(Icons.post_add, color: Colors.white,),
  Icon(Icons.view_carousel_rounded,color: Colors.white,),
  Icon(Icons.group, color: Colors.white,),
  Icon(Icons.edit, color: Colors.white,),
  Icon(Icons.delete, color: Colors.white,),
  Icon(Icons.class_, color: Colors.white,),

];

List<String> actionTitleList = [
  'Take Attendance',
  'View Attendance',
  'View Students',
  'Edit Classroom',
  'Delete Classroom',
  'View Details',
];
