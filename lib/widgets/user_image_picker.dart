import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UserImagePicker extends StatefulWidget {
  UserImagePicker(this.imagePickFn);

  final void Function(File pickedImage) imagePickFn;

  @override
  _UserImagePickerState createState() => _UserImagePickerState();
}

class _UserImagePickerState extends State<UserImagePicker> {
  File _pickedImage;

  // void _pickImage() async {
  //   final pickedImageFile = await ImagePicker.pickImage(
  //     source: ImageSource.camera,
  //     imageQuality: 50,
  //     maxWidth: 150,
  //   );
  //   setState(() {
  //     _pickedImage = pickedImageFile;
  //   });
  //   widget.imagePickFn(pickedImageFile);
  // }

  void _showPicker() async {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () async {
                        final pickedImageFile = await ImagePicker.pickImage(
                          source: ImageSource.gallery,
                          imageQuality: 50,
                          maxWidth: 150,
                        );
                        setState(() {
                          _pickedImage = pickedImageFile;
                        });
                        widget.imagePickFn(pickedImageFile);
                        Navigator.of(bc).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () async {
                      final pickedImageFile = await ImagePicker.pickImage(
                        source: ImageSource.camera,
                        imageQuality: 50,
                        maxWidth: 150,
                      );
                      setState(() {
                        _pickedImage = pickedImageFile;
                      });
                      widget.imagePickFn(pickedImageFile);
                      Navigator.of(bc).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }



  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      child:  Align(
          alignment: Alignment.bottomRight,
          child: InkWell(
            onTap: _showPicker,
            child: CircleAvatar(
              radius: 18,
              child: Icon(Icons.add),
            ),
          ),
      ),
      radius: 50,
      backgroundColor: Colors.grey,
      backgroundImage:
      _pickedImage != null ? FileImage(_pickedImage) : AssetImage('assets/images/noimg.png'),
    );
  }
}