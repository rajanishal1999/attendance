import 'package:attendance/provider/validate_attendance.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';


class AtttendanceTime extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        TextButton.icon(
            onPressed: () {},
            icon: Icon(Icons.date_range, color: Theme.of(context).primaryColor,),
            label: Consumer<ValidateAttendance>(
              builder: (ctx, atted, _) {
                return Text(atted.getAttendanceTime() == null ? 'Default Will be current time' :
                  DateFormat('dd/MM/yy-hh:mm a').format(atted.getAttendanceTime()),
                  style: TextStyle(color: Colors.black, fontSize: 15),);
              },

            ),
        ),
        TextButton(onPressed: () async {
          final timePicked = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.now(),
          );
          if(timePicked != null) {
            Provider.of<ValidateAttendance>(context, listen: false).setAttendanceTime(timePicked);
          }
        }, child: Text('Choose Time')),
      ],
    );
  }
}


// DateFormat('dd/MM/yy-hh:mm a').format(Provider.of<ValidateAttendance>(context).getAttendanceTime())