import 'dart:math';

import 'package:attendance/provider/validate_attendance.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../global_widgets.dart';



class AttendanceValidateReport extends StatefulWidget {
  final int totalNoStudent;
  final String currentUserId;
  final String classroomId;
  AttendanceValidateReport({this.totalNoStudent, this.currentUserId, this.classroomId});

  @override
  _AttendanceValidateReportState createState() => _AttendanceValidateReportState();
}

class _AttendanceValidateReportState extends State<AttendanceValidateReport> {
  var isLoading = false;


  void submitAttendance(BuildContext context) async {
    setState(() {
      isLoading = true;
    });
    var range = new Random();
    await Firestore.instance.collection('attendance').document(widget.currentUserId).collection('attendance_list').add(
        {
          'colorValue': range.nextInt(randomColorsToBeChoose.length),
          'cretedAt': Timestamp.now(),
          'iso8601timestamp':DateTime.now(),
          'classroomId' :widget.classroomId,
          'data':Provider.of<ValidateAttendance>(context, listen: false).getAttendanceList(),
          'totalNoOfStudents': Provider.of<ValidateAttendance>(context, listen: false).gettotalNoOfStudents(),
          'noOfPresent' : Provider.of<ValidateAttendance>(context, listen: false).noOfPresent(),
          'noOfAbsent' : Provider.of<ValidateAttendance>(context, listen: false).noOfAbsent(),
          'noOfNotCame' : Provider.of<ValidateAttendance>(context, listen: false).noOfNotcame(),
          'attendanceTime':  Provider.of<ValidateAttendance>(context, listen: false).getAttendanceTime() == null ? DateTime.now().toIso8601String() : Provider.of<ValidateAttendance>(context, listen: false).getAttendanceTime().toIso8601String(),
        });
    setState(() {
      isLoading = false;
    });
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<ValidateAttendance>(context, listen: false).totalNoOfStudents(widget.totalNoStudent);
    return Container(
      color: Theme.of(context).cardColor,
      margin: EdgeInsets.only(right: 8, left: 8),
      padding: const EdgeInsets.all(8.0),
      child: isLoading ? Center(child: CircularProgressIndicator(),) :Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Total No of Students: '),
                Text(widget.totalNoStudent.toString(), style: TextStyle(color: Theme.of(context).accentColor, fontSize: 16),),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  Text('Present: '),
                  Consumer<ValidateAttendance>(
                    builder: (ctx, atted, _) {
                      return Text(atted.noOfPresent().toString(), style: TextStyle(color: Theme.of(context).accentColor, fontSize: 15),);
                    },
                    child: Text('0', style: TextStyle(color: Theme.of(context).accentColor, fontSize: 15),),
                  ),
                ],
              ),
              Row(
                children: [
                  Text('Absent: '),
                  Consumer<ValidateAttendance>(
                    builder: (ctx, atted, _) {
                      return Text(atted.noOfAbsent().toString(), style: TextStyle(color: Theme.of(context).accentColor, fontSize: 15),);
                    },
                    child: Text('0', style: TextStyle(color: Theme.of(context).accentColor, fontSize: 15),),
                  ),
                ],
              ),
              Row(
                children: [
                  Text('Not Came: '),
                  Consumer<ValidateAttendance>(
                    builder: (ctx, atted, _) {
                      return Text(atted.noOfNotcame().toString(), style: TextStyle(color: Theme.of(context).accentColor, fontSize: 15),);
                    },
                    child: Text('0', style: TextStyle(color: Theme.of(context).accentColor, fontSize: 15),),
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                onPressed: Provider.of<ValidateAttendance>(context).validateToSave() ? () {
                  submitAttendance(context);
                } : null,
                child: Text('Submit')
            ),
          ),
        ],
      ),
    );
  }
}


