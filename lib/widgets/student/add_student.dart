import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../global_widgets.dart';

class AddStudent extends StatefulWidget {
  final isEdit;
  final String receivedName;
  final String receivedRoll;
  final String docId;
  final int tileColor;
  AddStudent({this.isEdit, this.receivedRoll, this.receivedName, this.docId, this.tileColor});
  @override
  _AddStudentState createState() => _AddStudentState();
}

class _AddStudentState extends State<AddStudent> {
  final _formKey = GlobalKey<FormState>();
  String studentName = '';
  String studentRollNumber = '';
  final _auth = FirebaseAuth.instance;


  Future<void> _addclassroom() async {
    var range = new Random();
    final user = await _auth.currentUser();

    final isValid = _formKey.currentState.validate();
    FocusScope.of(context).unfocus();


    if (isValid) {
      _formKey.currentState.save();
      if(!widget.isEdit) {
        await Firestore.instance.collection('students').document(user.uid).collection('students_list').add(
            {
              'student_name': studentName,
              'student_roll_number': int.parse(studentRollNumber),
              'classroomDocId': widget.docId,
              'CreatedAt': Timestamp.now(),
              'colorValue' : range.nextInt(randomColorsToBeChoose.length),
              'iso8601timestamp': DateTime.now().toIso8601String(),
            });
      }
      if(widget.isEdit){
        if(widget.receivedName == studentName && widget.receivedRoll == studentRollNumber) {
          Navigator.of(context).pop();
          return;

        }
        await Firestore.instance.collection('students').document(user.uid).collection('students_list').document(widget.docId).updateData(
            {
              'student_name': studentName,
              'student_roll_number': int.parse(studentRollNumber),
              'CreatedAt': Timestamp.now(),
              'iso8601timestamp': DateTime.now().toIso8601String(),
            });
      }
      Navigator.of(context).pop();
    }


  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(widget.isEdit ? 'Edit Student' :'Add Student', style: TextStyle(
                    color: widget.isEdit ? randomColorsToBeChoose[widget.tileColor] : Colors.blue,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),),
              ),
              TextFormField(
                maxLines: 1,
                initialValue: widget.isEdit ? widget.receivedName : null,
                key: ValueKey('student_name'),
                autocorrect: false,
                textCapitalization: TextCapitalization.sentences,
                enableSuggestions: true,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Student name is Empty !';
                  }
                  if (value.length >= 20) {
                    return 'Student name is too big !';
                  }
                  if (value.length < 4) {
                    return 'Student name is too short !';
                  }
                  return null;
                },
                onSaved: (value) {
                  studentName = value;
                },
                decoration: InputDecoration(
                    labelText: 'Student name'
                ),
              ),
              TextFormField(
                maxLines: 1,
                initialValue: widget.isEdit ? widget.receivedRoll : null,
                key: ValueKey('student_roll_number'),
                autocorrect: false,
                keyboardType: TextInputType.number,
                textCapitalization: TextCapitalization.none,
                enableSuggestions: true,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Roll Number is Empty !';
                  }
                  if (value.length >= 13) {
                    return 'Roll Number is too big !';
                  }
                  if (value.length <= 11) {
                    return 'Roll Number is too short !';
                  }
                  return null;
                },
                onSaved: (value) {
                  studentRollNumber = value;
                },
                decoration: InputDecoration(
                    labelText: 'Roll Number'
                ),
              ),
              SizedBox(height: 12,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(onPressed: () {
                    Navigator.of(context).pop();
                  }, child: Text('Cancel')),
                  TextButton(
                      onPressed: _addclassroom,
                      child: Text('Ok')
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

