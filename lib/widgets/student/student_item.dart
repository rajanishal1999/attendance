import 'package:attendance/provider/long_press_provider.dart';
import 'package:attendance/widgets/student/add_student.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../global_widgets.dart';


class StudentItem extends StatelessWidget {
  final String name;
  final String rollNo;
  final String createdOn;
  final int tileColor;
  final String currentUserId;
  final String docId;
  StudentItem({this.tileColor, this.name, this.createdOn, this.rollNo, this.currentUserId, this.docId});








  void onTapMethod(BuildContext context) {
    Provider.of<LongPressProvider>(context, listen: false).addToLongPress(docId.toString(), {'name':name, 'rollNo':rollNo, 'DocId':docId, 'tileColor':tileColor.toString()});

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 6),
      child: Consumer<LongPressProvider>(
        builder: (ctx, longP, _) {
          return ListTile(
            leading: CircleAvatar(
              backgroundColor: randomColorsToBeChoose[tileColor],
              child: Text(name[0].toString().toUpperCase()),
            ),
            title: Text(name),
            subtitle:Text(rollNo),
            onTap: () => onTapMethod(context),
            // trailing: PopupMenuButton(
            //   onSelected: (value) {
            //     if(value == 'deletestudent') {
            //       _deleteStudent(context);
            //     }
            //     if(value == 'editstudent') {
            //       //_editStudentAction(context);
            //     }
            //   },
            //   icon: Icon(Icons.more_vert),
            //   itemBuilder: (_) => [
            //     PopupMenuItem(
            //       child: TextButton.icon(icon: Icon(Icons.edit, color: randomColorsToBeChoose[tileColor],), label: Text('Edit Students', style: TextStyle(color: Colors.black),)),
            //       value: 'editstudent',
            //     ),
            //     PopupMenuItem(
            //       child: TextButton.icon(icon: Icon(Icons.delete, color: Theme.of(context).errorColor,), label: Text('Delete Students', style: TextStyle(color: Colors.black),)),
            //       value: 'deletestudent',
            //     ),
            //   ],
            // ),
            onLongPress: () {
              Provider.of<LongPressProvider>(context, listen: false).startLongPress();
              Provider.of<LongPressProvider>(context, listen: false).addToLongPress(docId.toString(), {'name':name, 'rollNo':rollNo, 'DocId':docId, 'tileColor':tileColor.toString()});
            },

            selected: longP.isItemselected(docId),
            selectedTileColor: Colors.grey[300],
          );
        },

      ),
    );
  }
}
