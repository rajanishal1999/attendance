import 'dart:math';

import 'package:attendance/widgets/global_widgets.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';




class AddClassroom extends StatefulWidget {
  final isEdit;
  final String receivedName;
  final String receivedDes;
  final String docId;
  final int tileColor;
  AddClassroom({this.isEdit, this.receivedDes, this.receivedName, this.docId, this.tileColor});
  @override
  _AddClassroomState createState() => _AddClassroomState();
}

class _AddClassroomState extends State<AddClassroom> {
  final _formKey = GlobalKey<FormState>();
  String classroomName = '';
  String classroomDescription = '';
  final _auth = FirebaseAuth.instance;


  Future<void> _addclassroom() async {
    var range = new Random();
    final user = await _auth.currentUser();

    final isValid = _formKey.currentState.validate();
    FocusScope.of(context).unfocus();


    if (isValid) {
      _formKey.currentState.save();
      if(!widget.isEdit) {
        await Firestore.instance.collection('classrooms').document(user.uid).collection('classroom_list').add({
          'classroom_name': classroomName,
          'classroom_description': classroomDescription,
          'CreatedAt': Timestamp.now(),
          'colorValue' : range.nextInt(randomColorsToBeChoose.length),
          'iso8601timestamp': DateTime.now().toIso8601String(),
        });
      }
     if(widget.isEdit){
       await Firestore.instance.collection('classrooms').document(user.uid).collection('classroom_list').document(widget.docId).updateData(
           {
             'classroom_name': classroomName,
             'classroom_description': classroomDescription,
             'CreatedAt': Timestamp.now(),
             'iso8601timestamp': DateTime.now().toIso8601String(),
           });
     }
      Navigator.of(context).pop();
    }


  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(widget.isEdit ? 'Edit Classroom' :'Add Classroom', style: TextStyle(
                  color: widget.isEdit ? randomColorsToBeChoose[widget.tileColor] : Colors.blue,
                  fontSize: 20,
                  fontWeight: FontWeight.bold
                ),),
              ),
              TextFormField(
                maxLines: 1,
                initialValue: widget.isEdit ? widget.receivedName : null,
                key: ValueKey('classroom_name'),
                autocorrect: false,
                textCapitalization: TextCapitalization.sentences,
                enableSuggestions: true,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Classroom name is Empty !';
                  }
                  if (value.length >= 20) {
                    return 'Classroom name is too big !';
                  }
                  if (value.length < 4) {
                    return 'Classroom name is too short !';
                  }
                  return null;
                },
                onSaved: (value) {
                  classroomName = value;
                },
                decoration: InputDecoration(
                    labelText: 'Classroom name'
                ),
              ),
              TextFormField(
                maxLines: 1,
                initialValue: widget.isEdit ? widget.receivedDes : null,
                key: ValueKey('classroom_description'),
                autocorrect: false,
                textCapitalization: TextCapitalization.none,
                enableSuggestions: true,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Description is Empty!';
                  }
                  if (value.length >= 30) {
                    return 'Description is too big!';
                  }
                  if (value.length < 4) {
                    return 'Description is too short !';
                  }
                  return null;
                },
                onSaved: (value) {
                  classroomDescription = value;
                },
                decoration: InputDecoration(
                    labelText: 'Description'
                ),
              ),
              SizedBox(height: 12,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(onPressed: () {
                    Navigator.of(context).pop();
                  }, child: Text('Cancel')),
                  TextButton(onPressed: _addclassroom, child: Text('Ok')),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
