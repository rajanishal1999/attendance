import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';




class LongPressProvider with ChangeNotifier {
  var _isInLongPress = false;
  List<String> _longPressItem = [];
  Map<String, String> _editBtnData = {};

  void startLongPress() {
    _isInLongPress = true;
    notifyListeners();
  }

  void stopLongPress() {
    _isInLongPress = false;
    _longPressItem = [];
    _editBtnData = {};
    notifyListeners();
  }

  get getLongPress {
    return _isInLongPress;
  }



  List<String> getdeleteBtnLst() {
    return _longPressItem;
  }


  void addToLongPress(String itemDocId, Map<String, String> edtData) {
    if(_isInLongPress) {
      if(_longPressItem.contains(itemDocId)) {
        _longPressItem.remove(itemDocId);
      }else {
        _longPressItem.add(itemDocId);
        _editBtnData = edtData;
      }
      notifyListeners();
    }
  }


  bool isItemselected(String docId) {
    if(_isInLongPress) {

      if(_longPressItem.contains(docId)) {return true;}

      else {
        return false;
      }
    }
    else {
      return false;
    }

  }

  bool isEditButton() {
    if(_longPressItem.length == 1) {
      return true;
    }
    else {
      return false;
    }
  }

  get getEditBtnData {
    return _editBtnData;
  }

}