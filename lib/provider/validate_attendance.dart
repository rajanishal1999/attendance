
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ValidateAttendance with ChangeNotifier{

  Map<String,Map<String, String>> _attendanceList = {};
  int _totalNoOfStudent = 0;
  TimeOfDay _pickedTime;

  void addAttendance(String value, String studentName, String rollNo) {
    _attendanceList[rollNo] = {
      'studentValue' :value,
      'studentName' :studentName,
      'rollNo' :rollNo,
    };
    notifyListeners();
  }

  void totalNoOfStudents(int total){
    _totalNoOfStudent = total;
  }

  int gettotalNoOfStudents() {
    return _totalNoOfStudent;
  }

  int noOfPresent() {
    int totalPresent = 0;
    _attendanceList.forEach((key, value) {
      if(value['studentValue'] == 'P') {
        totalPresent++;
      }
    });
    return totalPresent;
  }

  int noOfAbsent() {
    int totalAbsent = 0;
    _attendanceList.forEach((key, value) {
      if(value['studentValue'] == 'A') {
        totalAbsent++;
      }
    });
    return totalAbsent;
  }

  int noOfNotcame() {
    int totalNotCame = 0;
    _attendanceList.forEach((key, value) {
      if(value['studentValue'] == 'N') {
        totalNotCame++;
      }
    });
    return totalNotCame;
  }

  Map<String,Map<String, String>> getAttendanceList() {
    return _attendanceList;
  }

  bool validateToSave() {
    int takenValue = noOfPresent() + noOfAbsent() + noOfNotcame();
    if(_totalNoOfStudent == takenValue) {
      return true;
    }
    else {
      return false;
    }

  }

  void setAttendanceTime(TimeOfDay pickedDateTime) {
    _pickedTime = pickedDateTime;
    notifyListeners();
  }

  DateTime getAttendanceTime() {
    DateTime timenow = new DateTime.now();
    if(_pickedTime == null) {
      return null;
    }
    DateTime timepickedtoObj =  DateTime(timenow.year, timenow.month, timenow.day, _pickedTime.hour, _pickedTime.minute);
    return timepickedtoObj;
  }


}