import 'package:attendance/provider/long_press_provider.dart';
import 'package:attendance/screens/student/students_screen.dart';
import 'package:attendance/screens/take_attendance/take_attendance.dart';
import 'package:attendance/screens/view_attendance/view_attendanceScreen.dart';
import 'package:attendance/widgets/add_classroom.dart';
import 'package:attendance/widgets/global_widgets.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';





class ClassroomItem extends StatelessWidget {
  final String itemName;
  final String itemDescription;
  final String docId;
  final String cretedTime;
  final int itemColor;
  final String currentUserId;
  ClassroomItem({this.itemColor, this.itemDescription, this.itemName,
    this.docId,this.cretedTime, this.currentUserId});




  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8, right: 8, left: 8),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
        border: Border.all(
          color: Colors.grey,
        ),
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Consumer<LongPressProvider>(
        builder: (ctx, longP, _) {
          return InkWell(
            onTap: () {
              if(longP.getLongPress) {
                longP.addToLongPress(docId, {'name':itemName, 'des':itemDescription, 'docId':docId, 'tileColor': itemColor.toString()});
              }
            },
            onLongPress: () {
              longP.startLongPress();
              longP.addToLongPress(docId, {'name':itemName, 'des':itemDescription, 'docId':docId, 'tileColor': itemColor.toString()});
            },
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: longP.getLongPress ? ! longP.isItemselected(docId) ? randomColorsToBeChoose[itemColor].shade100 : randomColorsToBeChoose[itemColor] : randomColorsToBeChoose[itemColor],
                    borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                  ),
                  child: ListTile(
                    leading: CircleAvatar(
                      //backgroundColor: Theme.of(context).cardColor,
                      backgroundColor: randomColorsToBeChoose[itemColor].shade100,
                      child: Text(itemName[0].toUpperCase()),
                    ),
                    title: Text(itemName, style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontSize: 18,
                        fontWeight: FontWeight.w500
                    ),),
                    subtitle: Text(itemDescription, style: TextStyle(
                      color: Theme.of(context).cardColor,
                    ),),

                  ),
                ),
                Column(
                  children: <Widget>[
                    ListTile(
                      onTap: () {
                        longP.stopLongPress();
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => StudentScreen(
                          name: itemName,
                          decription: itemDescription,
                          createdon: cretedTime,
                          tileColor: itemColor,
                          classroomDocId: docId,
                          currentUserId: currentUserId,
                          appBarColor: itemColor,
                        )));
                      },
                      leading: Image.asset('assets/images/attendance_person.png'),
                      title: Text('Students'),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => TakeAttendance(
                          currentUserId: currentUserId,
                          name: itemName,
                          docId: docId,
                          createdOn: cretedTime,
                          description: itemDescription,
                          colorValue: itemColor,
                        )));
                      },
                      leading: Image.asset('assets/images/tickinto.png'),
                      title: Text('Take Attendance'),
                    ),
                    ListTile(

                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ViewAttendance(classroomId: docId, currentUid: currentUserId,)));
                      },
                      leading: Image.asset('assets/images/view_attendance.png'),
                      title: Text('View Attendance'),
                    ),
                  ],
                ),

              ],
            ),
          );
        },
      ),
    );
  }
}
